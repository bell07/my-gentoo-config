#!/bin/sh
export CLUTTER_BACKEND=wayland
export SDL_VIDEODRIVER=wayland

MY_LOG="$HOME/.my-wayfire-session.log"
[[ -f "$MY_LOG" ]] && mv "$MY_LOG" "$MY_LOG".bak

##### Place for own actions
##### Place for own actions - end

dbus-launch /usr/bin/wayfire-session > "$MY_LOG" 2>&1
