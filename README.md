#  My default settings that I prefer to use on all systems

# the files are installed my-gentoo-config ebuild from my (bell07) overlay

Folder | Target path | Description
----- | ----- | -----
portage | /etc/portage | Portage settings that cannot be in overlay profile
security/limits.d  | /etc/security | More relaxed limits for user, primary for gaming
sysctl.d | /etc/sysctl.d | System performance settings, primary for gaming
wayfire | /etc/skel/.wayfire; ~/.wayfire | Wayfire desktop settings for users
session-scripts | /etc/my-session-scripts | Session start scripts to be used in greetd
xorg.conf.d | /etc/X11/xorg.conf.d | X11 configuration (German keyboard)
